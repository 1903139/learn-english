import 'package:flutter/material.dart';
import 'package:learn_english/utilities/show_error_dialog.dart';
import 'package:learn_english/model/Questions.dart';
import 'package:learn_english/model/ScoreScreen.dart';

class QuestionController {
  // initialize question list from sources
  late final List<Question> _questions;

  QuestionController({required List data}) {
    _questions = data
        .map(
          (question) => Question(
            id: question['id'],
            question: question['question'],
            options: question['options'],
            answer: question['answer_index'],
          ),
        )
        .toList();
  }

  // returns the question list
  List<Question> get questions => this._questions;

  bool _isAnswerd = false;
  bool get isAnswered => this._isAnswerd;

  late int _correctAns;
  int get correcAns => this._correctAns;

  late int _selectedAns;
  int get selectedAns => this._selectedAns;

  int _numOfCorrectAns = 0;
  int get numOfCorrectAns => this._numOfCorrectAns;

  final _pageController = PageController();
  PageController get pageController => this._pageController;

  int _questionNumber = 1;
  int get questionNumber => this._questionNumber;

  List<Question> wrongAnswer = [];

  showrongAnswerError(Question question, BuildContext context) async {
    // final snackBar = SnackBar(
    //   content: Text('Correct answer: ${question.options[question.answer]}'),
    //   action: SnackBarAction(
    //     label: "Ok",
    //     onPressed: () {
    //       // goes to the next question
    //       nextQuestion(context);
    //     },
    //   ),
    // );
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Color.fromARGB(255, 234, 213, 213),
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: const [
                      Icon(
                        Icons.highlight_off,
                        color: Colors.red,
                        size: 20,
                      ),
                      Text('ভুল উত্তর ',
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20)),
                    ],
                  ),
                  const SizedBox(height: 10),
                  const Text('সঠিক উত্তর:',
                      style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                          fontSize: 16)),
                  const SizedBox(height: 5),
                  Text(question.options[question.answer],
                      style: TextStyle(fontSize: 16)),
                  const SizedBox(height: 10),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text('বুঝতে পেরেছি',
                            style: TextStyle(fontWeight: FontWeight.bold))),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void checkAns(
      Question question, int selectedIndex, BuildContext context) async {
    // when user select an option and press the submit button it will run
    _isAnswerd = true;
    _correctAns = question.answer;
    _selectedAns = selectedIndex;

    if (_correctAns == _selectedAns) {
      _numOfCorrectAns++;
      // goes to the next question
      nextQuestion(context);
    } else {
      // store the wrong answer
      wrongAnswer.add(question);
      await showrongAnswerError(question, context);
      nextQuestion(context);
    }
  }

  void nextQuestion(BuildContext context) {
    // move to the next question
    if (_questionNumber < _questions.length) {
      _questionNumber++;
      _isAnswerd = false;
      _pageController.nextPage(
        duration: Duration(milliseconds: 350),
        curve: Curves.ease,
      );
    } else {
      // show score screen
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => ScoreScreen(
                    controller: this,
                  )),
          (route) => false);
    }
  }
}
