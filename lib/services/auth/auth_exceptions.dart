// login exceptions

class UserNotFoundAuthException implements Exception {}

class WrongPasswordAuthException implements Exception {}

class UserNotLoggedInAuthException implements Exception {}


// registration exceptions

class InvalidEmailAuthException implements Exception {}

class WeakPasswordAuthException implements Exception {}

// generic exception

class GenericAuthExceptoin implements Exception {}
