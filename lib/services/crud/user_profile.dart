import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseAlreadyOpenException implements Exception {}

class DatabaseNotOpenException implements Exception {}

class UnableToGetDocumentDirector implements Exception {}

class CouldNotDeleteUserException implements Exception {}

class UserAlreadyExistsException implements Exception {}

class UserNotFoundException implements Exception {}

class CouldNotFindUserException implements Exception {}

class CouldNotDeleteProfileException implements Exception {}

class UserProfile {
  Database? _db;

  Future<void> deleteProfile({required int id}) async {
    final db = _getDatabaseOrThrow();

    final deleteCount = await db.delete(
      userProfileTable,
      where: 'id = ?',
      whereArgs: [id],
    );

    if (deleteCount == 0) {
      throw CouldNotDeleteProfileException();
    }
  }

  Future<DatabaseUserProfile> createProfile(DatabaseUser owner) async {
    final db = _getDatabaseOrThrow();

    final dbUser = await getUser(email: owner.email);

    if (owner != dbUser) {
      throw CouldNotFindUserException();
    }
    int score = 0;
    String username = 'rafsan';

    final profileId = await db.insert(userProfileTable, {
      userIdColumn: owner.id,
      scoreColumn: score,
      usernameColumn: username,
      isSyncedWithCloudColumn: 1,
    });

    return DatabaseUserProfile(
      id: profileId,
      userId: owner.id,
      score: score,
      isSyncedWithCloud: true,
      username: username,
    );
  }

  Future<DatabaseUser> getUser({required String email}) async {
    final db = _getDatabaseOrThrow();

    final result = await db.query(userTable,
        limit: 1, where: 'email = ?', whereArgs: [email.toLowerCase()]);

    if (result.isEmpty) {
      throw UserNotFoundException();
    } else {
      return DatabaseUser.fromRow(result.first);
    }
  }

  Future<DatabaseUser> createUser({required String email}) async {
    final db = _getDatabaseOrThrow();

    final result = await db.query(
      userTable,
      limit: 1,
      where: 'email = ?',
      whereArgs: [email.toLowerCase()],
    );

    if (result.isNotEmpty) {
      throw UserAlreadyExistsException();
    } else {
      final id = await db.insert(userTable, {
        emailColumn: email.toLowerCase(),
      });

      return DatabaseUser(id: id, email: email);
    }
  }

  Future<void> deleteUser({required String email}) async {
    final db = _getDatabaseOrThrow();

    final deletedCount = await db.delete(userTable,
        where: 'email = ?', whereArgs: [email.toLowerCase()]);

    if (deletedCount != 1) {
      throw CouldNotDeleteUserException();
    }
  }

  Database _getDatabaseOrThrow() {
    final db = _db;
    if (db == null) {
      throw DatabaseNotOpenException();
    } else {
      return db;
    }
  }

  Future<void> open() async {
    if (_db != null) {
      throw DatabaseAlreadyOpenException();
    }

    try {
      final docsPath = await getApplicationDocumentsDirectory();
      final dbPath = join(docsPath.path, dbName);
      final db = await openDatabase(dbPath);
      _db = db;

      // create the user table
      await db.execute(createUserTable);

      // create user profile table
      db.execute(createUserProfileTable);
    } on MissingPlatformDirectoryException {
      throw UnableToGetDocumentDirector();
    }
  }

  Future<void> close() async {
    final db = _db;
    if (db == null) {
      throw DatabaseNotOpenException();
    } else {
      await db.close();
      _db = null;
    }
  }
}

@immutable
class DatabaseUser {
  final int id;
  final String email;

  DatabaseUser({
    required this.id,
    required this.email,
  });

  DatabaseUser.fromRow(Map<String, Object?> map)
      : id = map[idColumn] as int,
        email = map[emailColumn] as String;

  @override
  String toString() {
    return 'Person ID: $id, email: $email';
  }

  @override
  bool operator ==(covariant DatabaseUser other) => id == other.id;

  @override
  int get hashCode => id.hashCode;
}

class DatabaseUserProfile {
  final int id;
  final int userId;
  final int score;
  final bool isSyncedWithCloud;
  final String username;

  DatabaseUserProfile({
    required this.id,
    required this.userId,
    required this.score,
    required this.isSyncedWithCloud,
    required this.username,
  });

  DatabaseUserProfile.fromRow(Map<String, Object?> map)
      : id = map[idColumn] as int,
        userId = map[userIdColumn] as int,
        score = map[scoreColumn] as int,
        isSyncedWithCloud =
            (map[isSyncedWithCloudColumn] as int) == 1 ? true : false,
        username = map[usernameColumn] as String;

  @override
  String toString() {
    return 'User profile, id: $id, username: $username, user_id: $userId, score: $score, isSyncedWithCloud: $isSyncedWithCloud';
  }

  @override
  bool operator ==(covariant DatabaseUserProfile other) => id == other.id;

  @override
  int get hashCode => id.hashCode;
}

const dbName = 'profile.db';
const userTable = 'user';
const userProfileTable = 'user_profile';
const idColumn = 'id';
const emailColumn = 'email';
const userIdColumn = 'user_id';
const scoreColumn = 'score';
const isSyncedWithCloudColumn = 'is_synced_with_cloud';
const usernameColumn = 'user_name';
const createUserProfileTable = '''
      CREATE TABLE "user_profile" (
      "id"	INTEGER NOT NULL,
      "user_id"	INTEGER NOT NULL,
      "score"	INTEGER NOT NULL,
      "is_synced_with_cloud"	INTEGER NOT NULL DEFAULT 0,
      "user_name"	TEXT NOT NULL,
      FOREIGN KEY("user_id") REFERENCES "user"("id"),
      PRIMARY KEY("id" AUTOINCREMENT)
    );
 ''';
const createUserTable = ''' 
      CREATE TABLE IF NOT EXISTS "user" (
      "id"	INTEGER NOT NULL,
      "email"	TEXT NOT NULL UNIQUE,
      PRIMARY KEY("id" AUTOINCREMENT)
    );
      ''';
