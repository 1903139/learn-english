import 'package:flutter/material.dart';

Widget textInput(String name, TextEditingController controller, bool isObscure,
    BuildContext context) {
  return Container(
    height: 55,
    width: MediaQuery.of(context).size.width - 70,
    child: TextFormField(
      obscureText: isObscure,
      controller: controller,
      autocorrect: false,
      enableSuggestions: false,
      decoration: InputDecoration(
          labelText: name,
          labelStyle: const TextStyle(
            fontSize: 16,
          ),
          enabledBorder: const OutlineInputBorder(),
          focusedBorder: const OutlineInputBorder()),
      onSaved: (String? value) {},
      validator: (String? value) {},
    ),
  );
}
