import 'package:flutter/material.dart';

Widget h1(String text) {
  return Text(text,
      style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold));
}

Widget p(String text) {
  return Text(text,
      style: TextStyle(
        fontSize: 20,
      ));
}

Widget b(String text) {
  return Text(text,
      style: TextStyle(
        fontWeight: FontWeight.bold,
      ));
}
