import 'package:flutter/material.dart';
import 'package:learn_english/utilities/text.dart';

Widget showSummery(List<Map> answers) {
  var col = Column(crossAxisAlignment: CrossAxisAlignment.start, children: []);
  for (var answer in answers) {
    col.children.add(b('ভুল উত্তর'));
    col.children.add(const SizedBox(height: 6));

    col.children.add(Text(answer['wrongAnswer']));
    col.children.add(const SizedBox(height: 10));

    col.children.add(b('সঠিক উত্তর'));
    col.children.add(const SizedBox(height: 6));

    col.children.add(Text(answer['rightAnswer']));
    col.children.add(const SizedBox(height: 10));
  }

  return col;
}
