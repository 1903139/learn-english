import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/material.dart';

import 'package:learn_english/constants/routes.dart';

import '../crud.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

enum MenuAction { logout }

class _ProfileState extends State<Profile> {
  late UserDB _storage;
  late UserProfile? _user;

  void getUser() async {
    _storage = UserDB(dbName: 'db.sqlite');
    final user = FirebaseAuth.instance.currentUser;
    _storage.open();
    //print(user?.uid);

    if (user != null) {
      _user = await _storage.getUser(user.uid);
    }

    // if (_user == null) {
    //   print('user not found');
    // } else {
    //   print('user found');
    // }
  }

  Future<DocumentSnapshot<Map<String, dynamic>>> getData() async {
    final user = FirebaseAuth.instance.currentUser;
    final docRef =
        FirebaseFirestore.instance.collection('users').doc(user?.uid);
    getUser();
    return docRef.get();
  }

  @override
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getData(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        final data;
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            //print('hello when done');
            data = snapshot.data;
            //print(data['name']);
            return Scaffold(
                appBar: AppBar(
                  title: const Text('Profile'),
                  actions: [
                    PopupMenuButton<MenuAction>(
                      onSelected: (value) async {
                        switch (value) {
                          case MenuAction.logout:
                            final shouldLogout =
                                await showLogoutDialog(context);
                            if (shouldLogout) {
                              FirebaseAuth.instance.signOut();
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                  loginRoute, (_) => false);
                            }
                            break;
                        }
                      },
                      itemBuilder: (context) {
                        return const [
                          PopupMenuItem<MenuAction>(
                              value: MenuAction.logout, child: Text('Logout'))
                        ];
                      },
                    )
                  ],
                ),
                body: SingleChildScrollView(
                    physics: const ScrollPhysics(),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${_user?.name}',
                              style: const TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold)),
                          const SizedBox(height: 10),
                          Text('${_user?.email}'),
                          const SizedBox(height: 20),
                          const Text('পরিসংখ্যান',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              )),
                          const SizedBox(height: 15),
                          const Text(
                              'রিডিং টেস্ট এ আপনার সর্বাধিক প্রাপ্ত পয়েন্ট সমূহ ',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                          ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: (_user?.score.length ?? 0) % 5,
                              itemBuilder: (context, index) {
                                return Text('${_user?.score[index]}');
                              }),
                          const SizedBox(height: 15),
                          const Text(
                              'লিসেনিং টেস্ট এ আপনার সর্বাধিক প্রাপ্ত পয়েন্ট সমূহ ',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )),
                          ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount:
                                  (_user?.listeningScore.length ?? 0) % 5,
                              itemBuilder: (context, index) {
                                return Text('${_user?.listeningScore[index]}');
                              }),
                          const SizedBox(height: 15),
                        ],
                      ),
                    )));

          default:
            return const Scaffold(
              body: Center(child: CircularProgressIndicator()),
            );
        }
      },
    );
  }

  Future<bool> showLogoutDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Sign out'),
            content: const Text('Are you sure you want to sign out'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: const Text('Sign out'),
              )
            ],
          );
        }).then((value) {
      if (value) {
        return true;
      } else {
        return false;
      }
    });
  }
}
