import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/pages/home.dart';
import 'package:learn_english/utilities/show_error_dialog.dart';

import '../../../utilities/text.dart';

class WritingTest extends StatefulWidget {
  const WritingTest({super.key});

  @override
  State<WritingTest> createState() => _WritingTestState();
}

class _WritingTestState extends State<WritingTest> {
  PageController pageController = PageController();
  TextEditingController textEditingController = TextEditingController();
  int score = 0;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Writing Test'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: PageView(
          controller: pageController,
          children: [
            QuestionBody(
                context: context,
                question: "যা শুনছেন, তা টাইপ করুন",
                answer: "ball",
                controller: textEditingController,
                pageController: pageController,
                isAudible: true,
                soundSource: "lesson_1_sound/ball.mp3"),
            QuestionBody(
                context: context,
                question: "যা শুনছেন, তা টাইপ করুন",
                answer: "cold",
                controller: textEditingController,
                pageController: pageController,
                isAudible: true,
                soundSource: "lesson_1_sound/cold.mp3"),
            QuestionBody(
                context: context,
                question: "যা শুনছেন, তা টাইপ করুন",
                answer: "hot",
                controller: textEditingController,
                pageController: pageController,
                isAudible: true,
                soundSource: "lesson_1_sound/hot.mp3"),
            Summery(context),
          ],
        ),
      ),
    );
  }

  void play(src) async {
    final player = AudioPlayer();
    await player.setSourceAsset("$src");
    await player.setVolume(1.0);
    player.resume();
  }

  Widget QuestionBody(
      {required BuildContext context,
      required String question,
      required String answer,
      required TextEditingController controller,
      required PageController pageController,
      bool? isAudible,
      String? soundSource}) {
    // if (isAudible != null) {
    //   play(soundSource);
    // }

    var col = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [],
    );

    col.children.add(h1(question));
    col.children.add(const SizedBox(height: 10));
    if (isAudible != null) {
      col.children.add(
        Align(
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () async {
              // add sound
              play(soundSource);
            },
            child: Icon(Icons.spatial_audio_rounded, size: 80),
          ),
        ),
      );
    }

    col.children.add(const SizedBox(height: 10));
    col.children.add(
      TextField(
        maxLines: 10,
        controller: controller,
        autocorrect: false,
        decoration: InputDecoration(
          hintText: 'ইংরেজি ভাষায় লিখুন',
          border: const OutlineInputBorder(),
        ),
      ),
    );
    col.children.add(const SizedBox(height: 10));
    col.children.add(Align(
        child: ElevatedButton(
            onPressed: () async {
              if (controller.text.toLowerCase().trim() == answer) {
                setState(() {
                  ++score;
                });
              } else {
                await showWrongAnswer(context, answer);
              }
              textEditingController.text = "";
              pageController.nextPage(
                  duration: (const Duration(milliseconds: 250)),
                  curve: Curves.ease);
            },
            child: Text('যাচাই করুন'))));

    return SingleChildScrollView(
      child: col,
    );
  }

  Widget Summery(BuildContext context) {
    return Column(
      children: [
        Text(score.toString()),
        ElevatedButton(
            onPressed: () async {
              await Navigator.of(context)
                  .pushNamedAndRemoveUntil(homeRoute, (route) => false);
            },
            child: Text('Back to home'))
      ],
    );
  }
}
