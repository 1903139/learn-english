/*
Description:
Shows the questino based on the article
  and summery of the result
 */

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/crud.dart';
import 'package:learn_english/utilities/text.dart';

import '../../../utilities/show_answers.dart';

var finalScore = 0;
var questionNumber = 0;
var quiz = Quiz();

class Quiz {
  var questions = [
    'Jessore is a district in:',
    'The worst influence of the British was:',
    'Prafulla Chandra Ray was greatly influenced by:',
    'He was surprised at:',
    '‘intelligentsia’ here means:',
  ];

  var choices = [
    ['West Bengal', 'Bangladesh', 'Bengal', 'Bengla'],
    ['tables', 'manners', 'table-manners', 'British values'],
    ['his brother', 'his uncle', 'his father', 'his cousin'],
    [
      'ever changing colors',
      'ever changing sounds and lights',
      'ever changing sounds and songs',
      'ever changing sights and sounds'
    ],
    [
      'intelligent animals',
      'intel television',
      'intelligent people',
      'a special detergent'
    ]
  ];

  var correctAnswers = [
    'Bangladesh',
    'table-manners',
    'his father',
    'ever changing sights and sounds',
    'intelligent people'
  ];
}

class Question1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QuestionState();
  }
}

class QuestionState extends State<Question1> {
  List<Map> answers = [];
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: Container(
            margin: const EdgeInsets.all(10.0),
            alignment: Alignment.topCenter,
            child: Column(
              children: <Widget>[
                const Padding(padding: EdgeInsets.all(20.0)),

                Container(
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Question ${questionNumber + 1} of ${quiz.questions.length}",
                        style: const TextStyle(fontSize: 22.0),
                      ),
                      Text(
                        "Score: $finalScore",
                        style: const TextStyle(fontSize: 22.0),
                      )
                    ],
                  ),
                ),

                const Padding(padding: EdgeInsets.all(10.0)),

                Text(
                  quiz.questions[questionNumber],
                  style: const TextStyle(
                    fontSize: 20.0,
                  ),
                ),

                const Padding(padding: EdgeInsets.all(10.0)),

                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    //button 1
                    MaterialButton(
                      minWidth: 120.0,
                      color: Colors.blueAccent,
                      onPressed: () {
                        if (quiz.choices[questionNumber][0] ==
                            quiz.correctAnswers[questionNumber]) {
                          debugPrint("Correct");
                          finalScore++;
                        } else {
                          debugPrint("Wrong");
                          String wrongAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.choices[questionNumber][0]}';
                          String rightAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.correctAnswers[questionNumber]}';
                          Map m = {
                            'wrongAnswer': wrongAnswer,
                            'rightAnswer': rightAnswer,
                          };

                          //quiz.correctAnswers[questionNumber];
                          answers.add(m);
                        }
                        updateQuestion(answers);
                      },
                      child: Text(
                        quiz.choices[questionNumber][0],
                        style: const TextStyle(
                            fontSize: 20.0, color: Colors.white),
                      ),
                    ),

                    //button 2
                    MaterialButton(
                      minWidth: 120.0,
                      color: Colors.blueAccent,
                      onPressed: () {
                        if (quiz.choices[questionNumber][1] ==
                            quiz.correctAnswers[questionNumber]) {
                          debugPrint("Correct");
                          finalScore++;
                        } else {
                          String wrongAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.choices[questionNumber][1]}';
                          String rightAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.correctAnswers[questionNumber]}';

                          Map m = {
                            'wrongAnswer': wrongAnswer,
                            'rightAnswer': rightAnswer,
                          };

                          //quiz.correctAnswers[questionNumber];
                          answers.add(m);
                        }
                        updateQuestion(answers);
                      },
                      child: Text(
                        quiz.choices[questionNumber][1],
                        style: const TextStyle(
                            fontSize: 20.0, color: Colors.white),
                      ),
                    ),
                  ],
                ),

                // Padding(padding: EdgeInsets.all(10.0)),

                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    //button 3
                    MaterialButton(
                      minWidth: 120.0,
                      color: Colors.blueAccent,
                      onPressed: () {
                        if (quiz.choices[questionNumber][2] ==
                            quiz.correctAnswers[questionNumber]) {
                          debugPrint("Correct");
                          finalScore++;
                        } else {
                          String wrongAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.choices[questionNumber][2]}';
                          String rightAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.correctAnswers[questionNumber]}';

                          Map m = {
                            'wrongAnswer': wrongAnswer,
                            'rightAnswer': rightAnswer,
                          };

                          //quiz.correctAnswers[questionNumber];
                          answers.add(m);
                        }
                        updateQuestion(answers);
                      },
                      child: Text(
                        quiz.choices[questionNumber][2],
                        style: const TextStyle(
                            fontSize: 20.0, color: Colors.white),
                      ),
                    ),

                    //button 4
                    MaterialButton(
                      minWidth: 120.0,
                      color: Colors.blueAccent,
                      onPressed: () {
                        if (quiz.choices[questionNumber][3] ==
                            quiz.correctAnswers[questionNumber]) {
                          debugPrint("Correct");
                          finalScore++;
                        } else {
                          String wrongAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.choices[questionNumber][3]}';
                          String rightAnswer =
                              '${quiz.questions[questionNumber]} ${quiz.correctAnswers[questionNumber]}';

                          Map m = {
                            'wrongAnswer': wrongAnswer,
                            'rightAnswer': rightAnswer,
                          };

                          //quiz.correctAnswers[questionNumber];
                          answers.add(m);
                        }
                        updateQuestion(answers);
                      },
                      child: Text(
                        quiz.choices[questionNumber][3],
                        style: const TextStyle(
                            fontSize: 20.0, color: Colors.white),
                      ),
                    ),
                  ],
                ),

                // Padding(padding: EdgeInsets.all(15.0)),
              ],
            ),
          ),
        ));
  }

  void resetQuiz() {
    setState(() {
      Navigator.pop(context);
      finalScore = 0;
      questionNumber = 0;
    });
  }

  // update to the next question or to the result section
  void updateQuestion(List<Map> answers) {
    setState(() {
      if (questionNumber == quiz.questions.length - 1) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Summary(
                      score: finalScore,
                      answers: answers,
                    )));
      } else {
        questionNumber++;
      }
    });
  }
}

// shows the summery of reading test
class Summary extends StatefulWidget {
  final int score;
  final List<Map> answers;
  Summary({Key? key, required this.score, required this.answers})
      : super(key: key) {
    // get the reading score of the current user
  }

  @override
  State<Summary> createState() => _SummaryState();
}

class _SummaryState extends State<Summary> {
  void storeScore(String? uid, int score) async {
    final storage = UserDB(dbName: 'db.sqlite');
    await storage.open();
    if (uid != null) {
      await storage.insertScore(uid, score);
    } else {
      print('invalid id ');
    }
  }

  @override
  void initState() {
    final user = FirebaseAuth.instance.currentUser;
    storeScore(user?.uid, widget.score);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 80),
              Text(
                "Final Score: ${widget.score}",
                style: const TextStyle(fontSize: 35.0),
              ),
              const SizedBox(height: 20),
              p('আপনি যেই উত্তরগুলো ভুল করেছেন'),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: showSummery(widget.answers)),
              const SizedBox(height: 30),
              MaterialButton(
                color: Colors.red,
                onPressed: () {
                  questionNumber = 0;
                  finalScore = 0;
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(homeRoute, (route) => false);
                },
                child: const Text(
                  "Back to home",
                  style: TextStyle(fontSize: 20.0, color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
