import 'package:flutter/material.dart';
import 'package:learn_english/pages/test/reading_test/Quiz1.dart';
//import 'package:flutter/services.dart'show rootBundle;

class ReadingTest extends StatefulWidget {
  const ReadingTest({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ReadingTest1();
  }
}

class ReadingTest1 extends State<ReadingTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reading Test"),
        centerTitle: true,
      ),
      body: Container(
        margin: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'নির্দিষ্ট সময়ের মধ্যে অনুচ্ছেদটি পড়ে প্রশ্নের সঠিক উত্তর দেয়ার চেষ্টা করুন ',
            ),
            const SizedBox(height: 16),
            ElevatedButton(
                onPressed: startReading,
                child: const Text(
                  "শুরু করুন",
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                ))
          ],
        ),
      ),
    );
  }

  void startReading() {
    setState(() {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Quiz1()));
    });
  }
}
