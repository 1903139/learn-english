import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'Question1.dart';

class Quiz1 extends StatefulWidget {
  const Quiz1({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ParagraphState();
  }
}

class ParagraphState extends State<Quiz1> {
  String data = '';
  double totalTime = 5;
  double time = 5;

  fetchFileData() async {
    String responseText;
    responseText = await rootBundle.loadString('assets/Reading1.txt');
    setState(() {
      data = responseText;
    });
  }

  @override
  void initState() {
    fetchFileData();
    Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        if (time != 0) {
          setState(() {
            time -= 1;
          });
        } else {
          timer.cancel();
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => Question1()));
        }
      },
    );
    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reading Test"),
        centerTitle: true,
        backgroundColor: Colors.pink,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                TimerProgressBar(
                  width: 200,
                  value: time,
                  totalValue: totalTime,
                ),
                Text(
                  data,
                  style: const TextStyle(
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TimerProgressBar extends StatelessWidget {
  final double width;
  final double value;
  final double totalValue;

  const TimerProgressBar(
      {super.key,
      required this.width,
      required this.value,
      required this.totalValue});

  @override
  Widget build(BuildContext context) {
    double ratio = value / totalValue;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(
          Icons.timer,
          color: Colors.grey,
        ),
        const SizedBox(height: 30),
        Stack(
          children: [
            Container(
              width: width,
              height: 10,
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(5),
              ),
            ),
            Material(
              elevation: 3,
              borderRadius: BorderRadius.circular(5),
              child: AnimatedContainer(
                height: 10,
                width: width * ratio,
                duration: const Duration(
                  microseconds: 500,
                ),
                decoration: BoxDecoration(
                  color: (ratio < 0.3)
                      ? Colors.red
                      : (ratio < .5)
                          ? Colors.amber
                          : Colors.lightGreen,
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
