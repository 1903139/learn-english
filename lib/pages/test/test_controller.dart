/*
Description

redirect to all the test model
such as reading, listening, speaking, writing, advance

*/

import 'package:flutter/material.dart';
import 'package:learn_english/pages/test/listening_test/ListeningTest.dart';
import 'package:learn_english/pages/test/reading_test/ReadingTest.dart';
import 'package:learn_english/pages/test/writing_test/writing_test.dart';

class TestController extends StatelessWidget {
  const TestController({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Test'),
        centerTitle: true,
      ),
      body: ListView(
        // shows the test item in a list view
        children: [
          // item1
          Card(
            child: ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return const ReadingTest();
                }));
              },
              title: const Text('Reading',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              trailing: const Icon(Icons.chrome_reader_mode_outlined),
            ),
          ),

          // item2
          Card(
            child: ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return const WritingTest();
                }));
              },
              title: const Text('Writing',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              trailing: const Icon(Icons.chrome_reader_mode_outlined),
            ),
          ),
          // item3
          Card(
            child: ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return const ListeningTest();
                }));
              },
              title: const Text('Listening',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              trailing: const Icon(Icons.chrome_reader_mode_outlined),
            ),
          ),
        ],
      ),
    );
  }
}
