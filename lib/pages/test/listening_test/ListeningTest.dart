import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:learn_english/main.dart';

import '../../../crud.dart';
import '../../../utilities/show_answers.dart';
import '../../../utilities/text.dart';

class ListeningTest extends StatefulWidget {
  const ListeningTest({super.key});

  @override
  State<ListeningTest> createState() => _ListeningTestState();
}

class _ListeningTestState extends State<ListeningTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Listening Test')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('কথোপকথনটি মনোযোগ দিয়ে শুনে প্রশ্নের উত্তর দেয়ার চেষ্টা করুন'),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return const Audio();
                  }));
                },
                child: Text('শুরু করুন '))
          ],
        ),
      ),
    );
  }
}

/*
  The audio class will begin the test and while playing the audio it 
  will show some question participant has to answer the question while 
  listening the clip.
 */
class Audio extends StatefulWidget {
  const Audio({super.key});

  @override
  State<Audio> createState() => _AudioState();
}

class _AudioState extends State<Audio> {
  List<TextEditingController> answerController = [];
  final player = AudioPlayer();

  final rightAnswer = ["9:30", "helendale", "central street", "792", "8:55"];

  @override
  void initState() {
    for (int i = 0; i < 5; i++) {
      answerController.add(TextEditingController());
    }
    print(answerController.length);
    playTest();
    super.initState();
  }

  @override
  void dispose() {
    player.stop();
    player.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Audio')),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  const Text('1. Express train leaves at ',
                      style: TextStyle(fontSize: 16.0)),
                  SizedBox(
                      child: TextField(
                        controller: answerController[0],
                      ),
                      width: 100),
                  const Text('am', style: TextStyle(fontSize: 16.0)),
                ],
              ),
              Row(
                children: [
                  const Text('2. Nearest Station is ',
                      style: TextStyle(fontSize: 16.0)),
                  SizedBox(
                      child: TextField(controller: answerController[1]),
                      width: 100),
                ],
              ),
              Row(
                children: [
                  const Text('3. Number 706 Bus goes to ',
                      style: TextStyle(fontSize: 16.0)),
                  SizedBox(
                      child: TextField(controller: answerController[2]),
                      width: 100),
                ],
              ),
              Row(
                children: [
                  const Text('4. Number ', style: TextStyle(fontSize: 16.0)),
                  SizedBox(
                      child: TextField(controller: answerController[3]),
                      width: 100),
                  const Text(' Bus goes to station.',
                      style: TextStyle(fontSize: 16.0)),
                ],
              ),
              Row(
                children: [
                  const Text('5. Earlier bus leaves at ',
                      style: TextStyle(fontSize: 16.0)),
                  SizedBox(
                      child: TextField(controller: answerController[4]),
                      width: 100),
                  const Text('am', style: TextStyle(fontSize: 16.0)),
                ],
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                  onPressed: () {
                    List<Map> answers = [];
                    List<String> ans = [];
                    int score = 0;

                    for (int i = 0; i < 5; i++) {
                      // modify answer
                      ans.add(answerController[i].text.toLowerCase());
                    }

                    //TODO: check for answer correctness and  pass the wrong answer to the result section

                    for (int i = 0; i < ans.length; i++) {
                      if (ans[i].toLowerCase() != rightAnswer[i]) {
                        String wAnswer = ans[i];
                        String rAnswer = rightAnswer[i];
                        answers.add({
                          "wrongAnswer": wAnswer,
                          "rightAnswer": rAnswer,
                        });
                      } else {
                        score++;
                      }
                    }

                    Navigator.of(context).pushAndRemoveUntil(
                        (MaterialPageRoute(builder: (context) {
                          return Result(
                            answers: answers,
                            score: score,
                          );
                        })),
                        (route) => false);
                  },
                  child: const Text('Submit'))
            ],
          ),
        ));
  }

  void playTest() async {
    await player.setSourceAsset('listening_test/listening_test_1.mp3');
    await player.setVolume(1.0);
    player.resume();
  }
}

/*
  Shows the result of the speaking test

 */
class Result extends StatefulWidget {
  final int score;
  final List<Map> answers;
  const Result({required this.answers, required this.score, super.key});

  @override
  State<Result> createState() => _ResultState();
}

class _ResultState extends State<Result> {
  void storeScore(String? uid, int score) async {
    final storage = UserDB(dbName: 'db.sqlite');
    await storage.open();
    if (uid != null) {
      await storage.insertListeningScore(uid, score);
    } else {
      print('invalid id ');
    }
  }

  @override
  void initState() {
    final user = FirebaseAuth.instance.currentUser;
    storeScore(user?.uid, widget.score);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.answers);
    return Scaffold(
        appBar: AppBar(title: const Text('Result')),
        body: SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                h1('Result'),
                p('Score: ${widget.score}'),
                const SizedBox(height: 20),
                p('আপনি যেই উত্তরগুলো ভুল করেছেন'),
                showSummery(widget.answers),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (context) {
                            return const Homepage();
                          }), (route) => false);
                        },
                        child: const Text('Back to Home')))
              ],
            ),
          ),
        ));
  }
}
