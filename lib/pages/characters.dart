import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

class Characters extends StatefulWidget {
  const Characters({Key? key}) : super(key: key);

  @override
  State<Characters> createState() => _CharactersState();
}

class _CharactersState extends State<Characters> {
  final alphabets = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z'
  ];
  final pronunciation = [
    'এই',
    'বী',
    'সী',
    'ডী',
    'ঈ',
    'এফ ',
    'জি',
    'এইচ',
    'আই',
    'জেই',
    'খেই',
    'এল',
    'এম',
    'এন',
    'ঔ',
    'ফি',
    'খিউ',
    'আ..র',
    'এস',
    'ঠী',
    'ইউ',
    'ভি',
    'ডাবল ইউ',
    'এক্স',
    'ওয়াই',
    'যেড',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Character')),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            physics: ScrollPhysics(),
            child: Column(
              children: [
                // ElevatedButton(
                //     onPressed: () {
                //       Navigator.of(context)
                //           .push(MaterialPageRoute(builder: (context) {
                //         return LearnCharacter();
                //       }));
                //     },
                //     child: Text('বর্ণমালা শিখুন ')),
                SizedBox(height: 16),
                GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: alphabets.length,
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 80,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                    ),
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () async {
                          final letter = alphabets[index];
                          final player = AudioPlayer();
                          await player
                              .setSourceAsset('character_sound/$letter.mp3');
                          await player.setVolume(1.0);
                          player.resume();

                          //print('$letter is clicked');
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 2,
                                color: const Color.fromARGB(216, 194, 212, 212),
                              ),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20))),
                          child: Center(
                              child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(alphabets[index],
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                const SizedBox(height: 5),
                                Text(pronunciation[index],
                                    style: const TextStyle(fontSize: 12)),
                              ],
                            ),
                          )),
                        ),
                      );
                    }),
              ],
            ),
          ),
        ));
  }
}

class LearnCharacter extends StatelessWidget {
  LearnCharacter({super.key});

  final data = [
    {
      'letter': 'A',
      'pronunciation': 'এই / আ',
      'example': ['Apple', 'A pen'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'B',
      'pronunciation': 'বী',
      'example': ['Baseball', 'Absent'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'C',
      'pronunciation': 'সী',
      'example': ['Cat', 'Cow'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'D',
      'pronunciation': 'ডী',
      'example': ['Doll', 'Dog'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'E',
      'pronunciation': 'ঈ',
      'example': ['Egg', 'End'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'F',
      'pronunciation': 'এফ',
      'example': ['Fun', 'Field'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'G',
      'pronunciation': 'জি',
      'example': ['Gold', 'Grow'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'H',
      'pronunciation': 'এইচ',
      'example': ['House', 'Hat'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'I',
      'pronunciation': 'আই',
      'example': ['Ink', 'Ill'],
      'sound': ['.', '.'],
    },
    {
      'letter': 'J',
      'pronunciation': 'জেই',
      'example': ['Jur', ''],
      'sound': ['.', '.'],
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Learn Character')),
      body: Text('Learn Character'),
    );
  }
}
