import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/crud.dart';

import '../utilities/show_error_dialog.dart';
import '../utilities/text_input.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late final TextEditingController _email;
  late final TextEditingController _password;

  @override
  void initState() {
    // TODO: implement initState
    _email = TextEditingController();
    _password = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Login')),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Welcome Back',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              const SizedBox(height: 20),
              OutlinedButton.icon(
                  onPressed: () {},
                  icon: const Icon(Icons.local_activity_outlined),
                  label: const Text('Continue with Google')),
              const SizedBox(height: 5),
              OutlinedButton.icon(
                  onPressed: () {},
                  icon: const Icon(Icons.local_activity_outlined),
                  label: const Text('Continue with facebook')),
              const SizedBox(height: 20),
              textInput('Email', _email, false, context),
              const SizedBox(height: 15),
              textInput('Password', _password, true, context),
              const SizedBox(height: 20),
              ElevatedButton(
                  onPressed: () async {
                    try {
                      final email = _email.text;
                      final password = _password.text;
                      await FirebaseAuth.instance.signInWithEmailAndPassword(
                        email: email,
                        password: password,
                      );

                      final user = FirebaseAuth.instance.currentUser;
                      if (user != null) {
                        UserDB storage = UserDB(dbName: 'sqlite.db');
                        storage.open();

                        final data = await FirebaseFirestore.instance
                            .collection('users')
                            .doc(user.uid)
                            .get();
                        storage.create(user.uid, data['name'] as String, email);
                      }

                      if (user?.emailVerified ?? false) {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          homeRoute,
                          (_) => false,
                        );
                      } else {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          varifyEmailRoute,
                          (_) => false,
                        );
                      }
                    } on FirebaseAuthException catch (e) {
                      if (e.code == 'user-not-found') {
                        await showErrorDialog(
                          context,
                          'User not found',
                        );
                      } else if (e.code == 'wrong-password') {
                        await showErrorDialog(
                          context,
                          'Wrong password',
                        );
                      } else {
                        await showErrorDialog(
                          context,
                          'Error: ${e.code}',
                        );
                      }
                    } catch (e) {
                      await showErrorDialog(
                        context,
                        e.toString(),
                      );
                    }
                  },
                  child: const Text('Sign in')),
              const SizedBox(height: 30),
              const Text('Not registered yet?'),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      registerRoute,
                      (_) => false,
                    );
                  },
                  child: const Text('Register here!')),
            ],
          ),
        ),
      ),
    );
  }
}
