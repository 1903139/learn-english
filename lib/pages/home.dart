import 'package:flutter/material.dart';
import 'package:learn_english/model/QuestionBody.dart';
import 'characters.dart' as ch;
import 'profile.dart';
import 'test/test_controller.dart';
import 'package:learn_english/model/Questions.dart';
import 'package:learn_english/pages/test/reading_test/ReadingTest.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  PageController pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: const [
          Lessons(),
          ch.Characters(),
          TestController(),
          Profile(),
        ],
        controller: pageController,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.language), label: 'Alphabet'),
          BottomNavigationBarItem(icon: Icon(Icons.book), label: 'Test'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
        ],
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
          pageController.jumpToPage(index);
        },
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber,
        unselectedItemColor: Colors.grey,
      ),
    );
  }
}

class Lessons extends StatefulWidget {
  const Lessons({Key? key}) : super(key: key);

  @override
  State<Lessons> createState() => _LessonsState();
}

class _LessonsState extends State<Lessons> {
  final title = ['প্রাথমিক', 'গণনা', 'অভিবাদন', 'খাবার', 'ভ্রমণ', 'পরিবার'];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: lessons.length,
        itemBuilder: (context, index) {
          return PopupMenuButton(
            position: PopupMenuPosition.under,
            onSelected: (String value) {
              if (value == 'practice') {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return QuestionBody(
                        sampleData: lessons[index],
                      );
                    },
                  ),
                );
              } else {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return const LessonTips();
                }));
              }
            },
            child: Card(
                child: ListTile(
                    title: Text(title[index],
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16)))),
            itemBuilder: (context) {
              return [
                //PopupMenuItem(child: Text('Tips'), value: 'tips'),
                PopupMenuItem(child: Text('Practice'), value: 'practice'),
              ];
            },
          );
        });
  }
}

class LessonTips extends StatefulWidget {
  const LessonTips({Key? key}) : super(key: key);

  @override
  State<LessonTips> createState() => _LessonTipsState();
}

class _LessonTipsState extends State<LessonTips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tips'),
      ),
      body: const Text('Tips about this lesson'),
    );
  }
}
