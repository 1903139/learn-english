import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:developer' as devtools show log;
import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/crud.dart';
import 'package:learn_english/services/crud/user_profile.dart';
import 'package:learn_english/utilities/show_error_dialog.dart';

import '../utilities/text_input.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  late final TextEditingController _username;
  late final TextEditingController _password;
  late final TextEditingController _email;

  @override
  void initState() {
    // TODO: implement initState
    _username = TextEditingController();
    _email = TextEditingController();
    _password = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _username.dispose();
    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Register')),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Get Started',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w500,
                  )),
              const SizedBox(height: 30),
              textInput('Username', _username, false, context),
              const SizedBox(height: 20),
              textInput('Email', _email, false, context),
              const SizedBox(height: 20),
              textInput('Password', _password, true, context),
              const SizedBox(height: 30),
              ElevatedButton(
                  onPressed: () async {
                    try {
                      final email = _email.text;
                      final password = _password.text;
                      final username = _username.text;

                      await FirebaseAuth.instance
                          .createUserWithEmailAndPassword(
                              email: email, password: password);

                      final user = FirebaseAuth.instance.currentUser;

                      // send information to the database
                      var db = FirebaseFirestore.instance;
                      await db.collection('users').doc(user?.uid).set({
                        'name': username,
                        'email': email,
                      });
                      await db.collection('scores').doc(user?.uid).set({
                        'userId': user?.uid,
                        'reading': 0,
                        'writing': 0,
                        'listening': 0,
                      });

                      // insert into offline database
                      final storage = UserDB(dbName: 'db.sqlite');
                      await storage.open();
                      bool isCreated =
                          await storage.create(user?.uid, username, email);
                      if (isCreated) print('user created');
                      await user?.sendEmailVerification();
                      Navigator.of(context).pushNamed(varifyEmailRoute);
                    } on FirebaseAuthException catch (e) {
                      if (e.code == 'invalid-email') {
                        showErrorDialog(
                          context,
                          'Invalid email address',
                        );
                      } else if (e.code == 'weak-password') {
                        showErrorDialog(
                          context,
                          'Weak password',
                        );
                      } else {
                        showErrorDialog(
                          context,
                          'Error: ${e.code}',
                        );
                      }
                    } catch (e) {
                      showErrorDialog(
                        context,
                        e.toString(),
                      );
                    }
                  },
                  child: const Text(
                    'Sign Up',
                  )),
              const SizedBox(height: 20),
              const Text('Already registered?'),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      loginRoute,
                      (_) => false,
                    );
                  },
                  child: const Text('Sign in here!')),
            ],
          ),
        ),
      ),
    );
  }
}
