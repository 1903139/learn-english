import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class UserProfile {
  final String userId;
  final String name;
  final String email;
  final List<int> score;
  final List<int> listeningScore;

  UserProfile(
      {required this.userId,
      required this.name,
      required this.email,
      required this.score,
      required this.listeningScore});
}

class UserDB {
  final String dbName;
  static Database? _db;
  final userTable = 'user';
  final readingScoreTable = 'reading_score';
  final listeningScoreTable = 'listening_score';

  UserDB({required this.dbName});

  Future<bool> open() async {
    if (_db != null) {
      // database already opened
      print('database already created');
      return true;
    }

    try {
      // open the database
      final directory = await getApplicationDocumentsDirectory();
      final path = '${directory.path}/${dbName}';

      Database db = await openDatabase(path);
      _db = db;

      // create table
      final user_table = '''
    CREATE TABLE IF NOT EXISTS $userTable  (
	"userId"	TEXT NOT NULL UNIQUE,
	"name"	TEXT NOT NULL,
	"email"	TEXT NOT NULL,
	PRIMARY KEY("userId")
);
 ''';

      final reading_score_table = '''
    CREATE TABLE IF NOT EXISTS $readingScoreTable (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
	  "reading_score_id"	TEXT,
	  "score"	INTEGER,
	  FOREIGN KEY("reading_score_id") REFERENCES "user"("userId")
);
''';

      final listening_score_table = '''
    CREATE TABLE IF NOT EXISTS $listeningScoreTable (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
	  "listening_score_id"	TEXT,
	  "score"	INTEGER,
	  FOREIGN KEY("listening_score_id") REFERENCES "user"("userId")
);
''';
      await db.execute(user_table);
      await db.execute(reading_score_table);
      await db.execute(listening_score_table);

      print('database created');
      // TODO: get existing data

      return true;
    } catch (e) {
      print('Exception creating database: ${e}');
      return false;
    }
  }

  // insert a user into the database
  Future<bool> create(String? uid, String name, String email) async {
    final db = _db;
    if (db == null || uid == null) {
      print('error in creating user');
      return false;
    }

    try {
      final id = await db
          .insert(userTable, {'userId': uid, 'name': name, 'email': email});
      return true;
    } catch (e) {
      print('Exception creating user: ${e}');
      return false;
    }
  }

  // insert score into the score table
  Future<bool> insertScore(String uid, int score) async {
    final db = _db;
    if (db == null) {
      print('error in inserting score db is null');
      return false;
    }

    try {
      final id = await db
          .insert(readingScoreTable, {'reading_score_id': uid, 'score': score});
      return true;
    } catch (e) {
      print('Exception inserting score ${e}');
      return false;
    }
  }

  // insert listening score to the listeing_score table
  Future<bool> insertListeningScore(String uid, int score) async {
    final db = _db;
    if (db == null) {
      print('error in inserting listening score db is null');
      return false;
    }

    try {
      final id = await db.insert(
          listeningScoreTable, {'listening_score_id': uid, 'score': score});
      return true;
    } catch (e) {
      print('Exception inserting listening_score ${e}');
      return false;
    }
  }

  // read from the user table
  Future<UserProfile?> getUser(String uid) async {
    final db = _db;
    if (db == null) {
      print('error to find user');
      return null;
    }

    try {
      // search the info of user into the db
      final profile = await db.query(
        'user',
        columns: [
          'userId',
          'name',
          'email',
        ],
        where: 'userId = ?',
        whereArgs: [uid],
      );

      final score = await db.query(
        readingScoreTable,
        columns: [
          'score',
        ],
        where: 'reading_score_id = ?',
        whereArgs: [uid],
        orderBy: 'score DESC',
      );

      final listeningScore = await db.query(listeningScoreTable,
          columns: ['score'],
          where: 'listening_score_id = ?',
          whereArgs: [uid],
          orderBy: "score DESC");

      // create a user
      String name = "";
      String email = "";
      List<int> scoreList = [];
      List<int> scoreListening = [];

      if (profile != [] && score != []) {
        name = profile[0]['name'] as String;
        email = profile[0]['email'] as String;

        for (int i = 0; i < score.length; i++) {
          scoreList.add(score[i]['score'] as int);
        }
      }

      for (var s in listeningScore) {
        scoreListening.add(s['score'] as int);
      }

      final user = UserProfile(
        name: name,
        email: email,
        userId: uid,
        score: scoreList,
        listeningScore: scoreListening,
      );

      // return the info of the user
      return user;
    } catch (e) {
      print('Exception  in finding user {$e}');
      return null;
    }
  }

  // close the db
  Future<bool> close() async {
    final db = _db;
    if (db == null) {
      return false;
    }

    await db.close();
    return true;
  }
}
