import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/pages/login.dart';
import 'package:learn_english/pages/register.dart';
import 'package:learn_english/pages/home.dart';
import 'dart:developer' as developer show log;
import 'firebase_options.dart';
import 'pages/varify_email_view.dart';
import 'crud.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const Homepage(),
      routes: {
        loginRoute: (context) => const Login(),
        registerRoute: (context) => const Register(),
        homeRoute: (context) => const Home(),
        varifyEmailRoute: (context) => const VerifyEmailView(),
      }));
}

class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  void createDatabase() async {
    final crudStorage = UserDB(dbName: 'db.sql');
    await crudStorage.open();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      ),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            // create sqlite database
            createDatabase();
            final user = FirebaseAuth.instance.currentUser;
            if (user != null) {
              if (user.emailVerified) {
                developer.log('varified');
              } else {
                return const VerifyEmailView();
              }
            } else {
              return const Login();
            }
            return const Home();

          default:
            return Scaffold(
                body: Center(child: const CircularProgressIndicator()));
        }
      },
    );
  }
}
