class Question {
  final int id, answer;
  final String question;
  final List<String> options;

  Question({
    required this.id,
    required this.question,
    required this.answer,
    required this.options,
  });
}

// const lessons = [
//   // lesson1 starts
//   // intro
//   {
//     'id': 0,
//     'question': 'Boy',
//     'options': [
//       'ছাত্র',
//       'ছাত্রী',
//       'ছেলে',
//       'মেয়ে',
//     ],
//     'answer_index': 2,
//   },
// ];

// const sample_data = [lessons];

const lessons = [
  [
    // lesson1 starts
    // intro
    {
      'id': 0,
      'question': 'বড়',
      'options': [
        'Big',
        'Small',
        'Tiny',
        'Ball',
      ],
      'answer_index': 0,
    },
    {
      'id': 1,
      'question': 'আমার',
      'options': [
        'Your',
        'Their',
        'My',
        'His',
      ],
      'answer_index': 2,
    },
    {
      'id': 2,
      'question': 'আমার অফিস বড়',
      'options': [
        'His Office is big',
        'My Office is big',
        "Ratul's office is big",
        'Their office is big',
      ],
      'answer_index': 1,
    },
    {
      'id': 3,
      'question': 'Hot',
      'options': [
        'গরম',
        'ঠাণ্ডা',
        'বাড়ি',
        'আমার',
      ],
      'answer_index': 0,
    },
    {
      'id': 4,
      'question': 'পানি',
      'options': [
        'Water',
        'Hot',
        'Cold',
        'House',
      ],
      'answer_index': 0,
    },
    {
      'id': 5,
      'question': 'আমার জল ঠাণ্ডা',
      'options': [
        'Your office is big',
        'My water is hot',
        'My water is cold',
        'My water is small',
      ],
      'answer_index': 2,
    },
    {
      'id': 6,
      'question': 'Name',
      'options': [
        'চা',
        'গরম',
        'বাড়ি ',
        'নাম',
      ],
      'answer_index': 3,
    },
    {
      'id': 7,
      'question': 'আমার নাম রাফসান',
      'options': [
        'Your name is Rafsan',
        'My name is Rafsan',
        'His name is Rafsan',
        'He is Rafsan',
      ],
      'answer_index': 1,
    },
  ],

  // lesson2 starts
  // counting
  [
    {
      'id': 0,
      'question': 'One',
      'options': [
        'এক',
        'দুই',
        'তিন',
        'চার',
      ],
      'answer_index': 0,
    },
    {
      'id': 1,
      'question': 'Two',
      'options': [
        'এক',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 2,
    },
    {
      'id': 2,
      'question': 'Three',
      'options': [
        'এক',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 3,
    },
    {
      'id': 3,
      'question': 'Four',
      'options': [
        'তিন',
        'এক',
        'চার',
        'দুই',
      ],
      'answer_index': 2,
    },
    {
      'id': 4,
      'question': 'There are two students',
      'options': [
        'সেখানে তিনজন ছাত্র আছে ',
        'সেখানে দুইজন ছাত্র আছে ',
        'সেখানে চারজন ছাত্র আছে',
        'সেখানে একজন ছাত্র আছে',
      ],
      'answer_index': 1,
    },
    {
      'id': 5,
      'question': 'I have one bag',
      'options': [
        'আমার পাঁচটি ব্যাগ আছে',
        'আমার আটটি ব্যাগ আছে',
        'আমার নয়টি ব্যাগ আছে',
        'আমার একটি ব্যাগ আছে',
      ],
      'answer_index': 1,
    },
    {
      'id': 6,
      'question': 'Five',
      'options': [
        'পাঁচ',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 0,
    },
    {
      'id': 7,
      'question': 'Six',
      'options': [
        'পাঁচ',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 0,
    },
  ],

  // lesson3 starts
  // greetings
  [
    {
      'id': 0,
      'question': 'Hello',
      'options': [
        'ওহে',
        'সকাল',
        'বিদায়',
        'সন্ধ্যা',
      ],
      'answer_index': 0,
    },
    {
      'id': 1,
      'question': 'Morning',
      'options': [
        'ওহে',
        'সকাল',
        'বিদায়',
        'সন্ধ্যা',
      ],
      'answer_index': 1,
    },
    {
      'id': 2,
      'question': 'Evening',
      'options': [
        'ওহে',
        'সকাল',
        'বিদায়',
        'সন্ধ্যা',
      ],
      'answer_index': 3,
    },
    {
      'id': 3,
      'question': 'Good Bye',
      'options': [
        'ওহে',
        'সকাল',
        'বিদায়',
        'সন্ধ্যা',
      ],
      'answer_index': 2,
    },
  ],

  // lesson4 starts
  // food
  [
    {
      'id': 0,
      'question': 'Hello',
      'options': [
        'পাঁচ',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 0,
    },
  ],

  // lesson5 starts
  // travel
  [
    {
      'id': 0,
      'question': 'Hello',
      'options': [
        'পাঁচ',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 0,
    },
  ],

  // lesson6 starts
  // family
  [
    {
      'id': 0,
      'question': 'Hello',
      'options': [
        'পাঁচ',
        'চার',
        'দুই',
        'তিন',
      ],
      'answer_index': 0,
    },
  ],
];

/*
  


 */