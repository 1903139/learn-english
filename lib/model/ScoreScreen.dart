import 'package:flutter/material.dart';
import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/controller/question_controller.dart';
import 'package:learn_english/model/Questions.dart';

class ScoreScreen extends StatefulWidget {
  final QuestionController controller;

  const ScoreScreen({Key? key, required this.controller}) : super(key: key);

  @override
  State<ScoreScreen> createState() => _ScoreScreenState();
}

class _ScoreScreenState extends State<ScoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Score')),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text('Score: ${widget.controller.numOfCorrectAns.toString()}',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil(homeRoute, (route) => false);
                    },
                    child: const Text('Back to lessons ')),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
