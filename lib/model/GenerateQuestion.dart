import 'package:flutter/material.dart';
import 'package:learn_english/constants/routes.dart';
import 'package:learn_english/controller/question_controller.dart';
import 'package:learn_english/model/Questions.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:learn_english/utilities/text.dart';

class GenerateQuestion extends StatefulWidget {
  final Question question;
  final QuestionController controller;
  const GenerateQuestion(
      {Key? key, required this.question, required this.controller})
      : super(key: key);

  @override
  State<GenerateQuestion> createState() => _GenerateQuestionState();
}

class _GenerateQuestionState extends State<GenerateQuestion> {
  bool isSelected = false;
  int selectedIndex = -1;
  late final player;
  late Question question;

  @override
  void initState() {
    player = AudioPlayer();
    question = widget.question;

    if (question.question.contains(RegExp('[A-Za-z]+'))) {
      print(question.question);
      play('lesson_1_sound/${question.question.toLowerCase()}.mp3');
    }
    super.initState();
  }

  void play(src) async {
    await player.setSourceAsset(src);
    await player.setVolume(1.0);
    player.resume();
  }

  Widget showQuestion(String question) {
    List<Widget> items = [];

    if (question.split(" ").length > 1) {
      items.add(h1(
        'নিচের বাক্যটি অনুবাদ করুন',
      ));
    } else {
      items.add(h1('নিচের শব্দটি অনুবাদ করুন'));
    }

    if (question.contains(new RegExp('[A-Za-z]+'))) {
      final qws = Row(
        children: [
          GestureDetector(
            child: Icon(Icons.spatial_audio),
            onTap: () {
              // add audio file
              play('lesson_1_sound/${question.toLowerCase()}.mp3');
            },
          ),
          const SizedBox(width: 10),
          p(question),
        ],
      );

      items.add(qws);
    } else {
      items.add(p(question));
    }

    var col = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [],
    );

    for (Widget item in items) {
      col.children.add(item);
      col.children.add(const SizedBox(height: 10));
    }
    return col;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            showQuestion(question.question),
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // show question

                      // Text(question.question,
                      //     style:
                      //         TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                      // const SizedBox(height: 50),

                      Container(
                        height: 200,
                        width: 200,
                        child: GridView.builder(
                          gridDelegate:
                              const SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 100,
                                  mainAxisSpacing: 10,
                                  crossAxisSpacing: 10),
                          itemBuilder: ((context, index) => ElevatedButton(
                                onPressed: () {
                                  isSelected =
                                      true; // confirms an option has been selected

                                  setState(() {
                                    selectedIndex = index;
                                  });

                                  String txt = question.options[index];
                                  if (txt.contains(new RegExp('[a-zA-Z]+'))) {
                                    txt = txt.toLowerCase();

                                    String temp = txt.replaceAll(' ', '_');
                                    print(temp);
                                    play('lesson_1_sound/${temp}.mp3');
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: (index == selectedIndex)
                                        ? Colors.amber
                                        : Colors.blue),
                                child: Text(question.options[index],
                                    style: const TextStyle(fontSize: 16)),
                              )),
                          itemCount: question.options.length,
                        ),
                      ),
                      const SizedBox(height: 30),
                      ElevatedButton(
                          onPressed: () {
                            // check whether an option is selected
                            if (!isSelected) {
                              // show snackbar to select an option
                            } else {
                              widget.controller
                                  .checkAns(question, selectedIndex, context);
                            }

                            // then check validity of answer
                            // if answer if correct then move to the next question
                            // else show the right answer then move to the next question
                          },
                          child: Text('Submit', style: TextStyle(fontSize: 16)),
                          style:
                              ElevatedButton.styleFrom(primary: Colors.purple))
                    ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
