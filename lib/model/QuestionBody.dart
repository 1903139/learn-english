import 'package:flutter/material.dart';
import 'package:learn_english/controller/question_controller.dart';
import 'package:learn_english/model/GenerateQuestion.dart';
import 'package:learn_english/model/Questions.dart';

class QuestionBody extends StatefulWidget {
  late final sampleData;
  QuestionBody({Key? key, required this.sampleData}) : super(key: key);
  @override
  State<QuestionBody> createState() => _QuestionBodyState();
}

class _QuestionBodyState extends State<QuestionBody> {
  late final questionController;
  @override
  void initState() {
    super.initState();
    questionController = QuestionController(data: widget.sampleData);
  }

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      physics: NeverScrollableScrollPhysics(),
      controller: questionController.pageController,
      itemCount: questionController.questions.length,
      itemBuilder: ((context, index) => GenerateQuestion(
          question: questionController.questions[index],
          controller: questionController)),
    );
  }
}
